/*
 * arrays1.h
 *
 *  Created on: Jan 20, 2018
 *      Author: abanoub
 */

#ifndef ARRAYS1_H_
#define ARRAYS1_H_

// include important libraries
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// fill an array with values begining with 0
void fillArray(int* input, long size);

// fill an array with random values
void randomArray(int* input, long size, int min, int max);

// print an array
void printArray(int* input, long size);

// add a number to all elements of an array
void arrayAdd(int* input, long size, int num);

// multiply all elements of an array with a number
void arrayMultiply(int* input, long size, int num);

// square all values of an array
void arraySqr(int* input, long size, int num);

// returns sum of all elements in an array
int arraySum(int* input, long size);

// returns product of all elements in an array
long arrayProduct(int* input, long size);

// returns sum of all elements in an array squared
int arraySqrSum(int* input, long size);

#endif /* ARRAYS1_H_ */
