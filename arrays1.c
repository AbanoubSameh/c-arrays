/*
 * arrays1.c
 *
 *  Created on: Jan 20, 2018
 *      Author: abanoub
 */

#include "arrays1.h"

int randd = 0;

// fills array with values
void fillArray(int* input, long size) {

	for (int i = 0; i < size; i++) {

		// put a number in each pointer
		*(input + i) = i;

	}

}

// generates random numbers
void randomArray(int* input, long size, int min, int max) {

	if(randd == 0){

		srand(time(NULL));
		randd = 1;

	}

	for (int i = 0; i < size; i++) {

		// puts a random number in each pointer
		*(input + i) = min + rand() % (max + 1 - min);

	}

}

// prints an array
void printArray(int* input, long size) {

	for (int i = 0; i < size; i++) {

		printf(" %d \n", *(input + i));

	}

}

// adds num to all elements
void arrayAdd(int* input, long size, int num) {

	for (int i = 0; i < size; i++) {

		*(input + i) += num;

	}

}

// multiplies num to all elements
void arrayMultiply(int* input, long size, int num) {

	for (int i = 0; i < size; i++) {

		*(input + i) *= num;

	}

}

// squares all elements
void arraySqr(int* input, long size, int num) {

	for (int i = 0; i < size; i++) {

		*(input + i) *= *(input + i);

	}

}

// sums all elements together
int arraySum(int* input, long size) {

	// int to store the sum
	int sum = 0;

	for (int i = 0; i < size; i++) {

		sum += *(input + i);

	}

	return sum;

}

// multiplies all elements together
long arrayProduct(int* input, long size) {

	// int to store product
	int product = 1;

	for (int i = 0; i < size; i++) {

		// skip multiplying if value is equal to zero
		if (*(input + i) == 0) {
			continue;
		}

		product *= *(input + i);

	}

	return product;

}

// squares all elements the sums them
int arraySqrSum(int* input, long size) {

	// int to store sum value
	int sum = 0;

	for (int i = 0; i < size; i++) {

		sum += (*(input + i) * *(input + i));

	}

	return sum;

}
