/*
 * homework.c
 *
 *  Created on: Jan 20, 2018
 *      Author: abanoub
 */

#include "arrays1.h"

int main(void) {

	int test[2];
	int test2[2];
	int test3[2];
	int test4[2];
	int test5[2];
	int test6[2];
	int test7[2];
	int test8[2];

	int size = sizeof(test) / 4;

	randomArray(test, size, 0, 10);
	randomArray(test2, size, 10, 20);
	randomArray(test3, size, 20, 30);
	randomArray(test4, size, 30, 40);
	randomArray(test5, size, 40, 50);
	randomArray(test6, size, 50, 60);
	randomArray(test7, size, 60, 70);
	randomArray(test8, size, 70, 80);

	printArray(test, size);
	printArray(test2, size);
	printArray(test3, size);
	printArray(test4, size);
	printArray(test5, size);
	printArray(test6, size);
	printArray(test7, size);
	printArray(test8, size);

//	arrayAdd(test, size, 273);

//	printArray(test, size);

}

